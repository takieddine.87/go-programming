package main

import (
	"fmt"
	"log"
)

type raizError struct {
	lat  string
	long string
	err  error
}

func (re raizError) Error() string {
	return fmt.Sprintf("error matemático: %v %v %v", re.lat, re.long, re.err)
}

func main() {
	res, err := raiz(-10.23)
	if err != nil {
		log.Println(err)
	}
	fmt.Println(res)
}

func raiz(f float64) (float64, error) {
	error := raizError {
		lat: "Heloo",
		long: "desde error",
		err: fmt.Errorf("no existe la raiz cuadrada de un numero negativo"), //Tambien puedo usar errors.New()
	}
	if f < 0 {
		return 0, error
	}
	return 42, nil
}

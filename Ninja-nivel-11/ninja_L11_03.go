package main

import (
	"fmt"
)

type errorPer struct {
	info string
}

func (ep errorPer) Error() string {
	return fmt.Sprintf("aquí está el error: %v", ep.info)
}

func main() {
	e1 := errorPer{
		info: "Helo mother fucker",
	}
	foo(e1) //Aunque foo solo acepte parametros de tipo *error*; Vemos que errorPer recibe el método Error() que
	// implementa implicitamente la interfaz error (vease https://go.dev/blog/error-handling-and-go), por lo que
	//podemos pasar nuestro e1 sin problema
}

func foo(e error) {
	fmt.Println(e.(errorPer).info)
}
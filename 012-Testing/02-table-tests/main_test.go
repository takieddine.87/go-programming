package main

import (
	"testing"
)

func TestMiSuma2(t *testing.T) {
	prueba := []struct {
		datos []int
		resultado int
	}{
		{[]int{4, 5, 6, 9}, 24},
		{[]int{4, 60, -1, 1}, 64},
		{[]int{3, 5, 2, 5}, 15},
		{[]int{4, 5, 6, 8}, 23},
	}

	for _, v := range prueba {
		if v.resultado != miSuma(v.datos...){
			t.Errorf("expected %d but have %d", v.resultado, miSuma(v.datos...))
		}
	}
}

package decir

import (
	"fmt"
	"testing"
)

func TestSaludar(t *testing.T) {
	s := Saludar("Eduardo")
	if s != "Bienvenido a benchmark: Eduardo" {
		t.Error("Expected", "Bienvenido a benchmark: Eduardo", "Got", s)
	}
}

func ExampleSaludar() {
	fmt.Println(Saludar("Takito"))
	//Output:
	//Bienvenido a benchmark: Takito
}

func BenchmarkSaludar(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Saludar("Takito")
	}
}
// Package decir nos permite saludar
package decir

import "fmt"

// Saludar nos permita saludas a una persona
func Saludar(s string) string {
	return fmt.Sprint("Bienvenido a benchmark: ", s)
}

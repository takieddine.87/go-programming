package main

import (
	"fmt"
	"gitlab.com/takieddine.87/go-programming/012-Testing/04-benchmark/03-greet/mystr"
)

func main() {
	fmt.Println(mystr.Greet("James"))
}

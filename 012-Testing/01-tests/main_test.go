package main

import "testing"

func TestMiSuma(t *testing.T)  {
	x := []int{3, 3, 4, 5, 6}
	if result := miSuma(x...); result != 21 {
		t.Errorf("expected %d but have %d", 21, result)
	}
}

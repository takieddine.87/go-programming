// Package mate nos ayuda a comprobar que sabes sumar
package mate

// Sum Suma una cantidad ilimitada de numeros enteros
func Sum(x ...int) int  {
	var s int
	for _, v := range x {
		s += v
	}

	return s
}

package main

import "fmt"

func main()  {
	c := make(chan int)

	go func() {
		c <- 42
		close(c)
	}()
	v, ok := <-c
	if ok {
		fmt.Println("Is ok")
		fmt.Println(v)
	}
	v, ok = <-c
	if !ok {
		fmt.Println("Is NOT ok")
		fmt.Println(v)
	}
}
package perrito

import (
	"fmt"
	"testing"
)

func TestYears(t *testing.T) {
	anios := struct {
		edades []int
		resultados []int
	}{
		[]int{6,5,8,2,3},
		[]int{42, 35, 56, 14, 21},
	}
	for i := 0; i < len(anios.edades) ; i++ {
		if resultado := Years(anios.edades[i]); resultado != anios.resultados[i]{
			t.Errorf("expected %d but have %d", anios.resultados[i], Years(anios.edades[i]))
		}
	}
}

func TestYearsTwo(t *testing.T) {
	anios := struct {
		edades []int
		resultados []int
	}{
		[]int{6,8,9},
		[]int{42, 56, 63},
	}
	for i := 0; i < len(anios.edades) ; i++ {
		if resultado := YearsTwo(anios.edades[i]); resultado != anios.resultados[i]{
			t.Errorf("expected %d but have %d", anios.resultados[i], YearsTwo(anios.edades[i]))
		}
	}
}

func BenchmarkYears(b *testing.B) {
	for i := 0; i < b.N ; i++ {
		Years(20)
	}
}

func BenchmarkYearsTwo(b *testing.B) {
	for i := 0; i < b.N ; i++ {
		YearsTwo(10)
	}
}

func ExampleYears() {
	fmt.Println(Years(10))
	// Output:
	// 70
}

func ExampleYearsTwo() {
	fmt.Println(YearsTwo(10))
	// Output:
	// 70
}
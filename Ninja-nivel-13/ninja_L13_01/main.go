package main
import (
	"fmt"
	"gitlab.com/takieddine.87/go-programming/Ninja-nivel-13/ninja_L13_01/perrito"
)

type canine struct {
	name string
	age  int
}

func main() {
	fido := canine{
		name: "Fido",
		age:  perrito.Years(10),
	}
	fmt.Println(fido)
	fmt.Println(perrito.YearsTwo(20))
}
package word

import (
	"fmt"
	"gitlab.com/takieddine.87/go-programming/Ninja-nivel-13/ninja_L13_02/quote"
	"testing"
)

func TestCount(t *testing.T) {
	frase := "Hola muy buenas tardes seniores"

	if resultat := Count(frase); resultat != 5 {
		t.Errorf("Expected %d have %d", 5, Count(frase))
	}
}

func TestUseCount(t *testing.T) {
	m := UseCount("one two three three three three")
	for k, v := range m {
		switch k {
		case "one":
			if v != 1 {
				t.Error("got", v, "want", 1)
			}
		case "two":
			if v != 1 {
				t.Error("got", v, "want", 1)
			}
		case "three":
			if v != 4 {
				t.Error("got", v, "want", 3)
			}
		}
	}
}

func ExampleCount() {
	fmt.Println(Count(quote.SunAlso))
	// Output:
	// 9
}

func ExampleUseCount() {
	xs := "one two three three three three"
	fmt.Println(UseCount(xs))
	//Output:
	//map[one:1 three:4 two:1]
}

func BenchmarkCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Count(quote.SunAlso)
	}
}

func BenchmarkUseCount(b *testing.B) {
	for i := 0; i < b.N; i++ {
		UseCount(quote.SunAlso)
	}
}

package main

import "fmt"

func main()  {
	ca := make(chan int, 2) //Este es un canal con buffer, es decir que podemos tener almacenado
	//uno o varios valores (segun el segundo parametro) dentro del canal
	ca<-42
	ca<-43
	fmt.Println(<-ca)//Imprimira el 42
	fmt.Println(<-ca)//imprimira el 43
}

package main

import "fmt"

func main()  {
	//Lo primero que hacemos es declarar el canal
	c := make(chan int)//chan es de channel et int, es el tipo de valor que va a estar enviando y recibiendo este canal
	//Hay dos tipos de canales, unos sin buffer y otros con buffer
	// c es un canal sin buffer
	// Los canales hay que imaginarselos como un tubo que conecta dos goroutinas. Los canales sin buffr
	//Solo las podemos usar cuando hay una goroutina que envia y la otra recibe.
	c <- 42
	fmt.Println(<-c)
	//Este programa va a morir e imprimira un error, no hay nigun goroutina que recibe el 42
}

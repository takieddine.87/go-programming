package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Numero de cpu's: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutinas: %v\n", runtime.NumGoroutine())
	var wg sync.WaitGroup

	wg.Add(2) //Ponemos el numero de goroutinas que queremos ejecutar
	go func() {
		fmt.Println("Hola desde la primera goroutina")
		wg.Done()
	}()
	go func() {
		fmt.Println("Hola desde la 2 goroutina")
		wg.Done()
	}()

	fmt.Printf("Numero de cpu's: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutinas: %v\n", runtime.NumGoroutine())
	wg.Wait()

	fmt.Println("Hola desde Main")
	fmt.Printf("Numero de cpu's: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de Goroutinas: %v\n", runtime.NumGoroutine())
}

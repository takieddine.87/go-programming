package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main()  {
	var wg sync.WaitGroup
	contador := 100
	wg.Add(contador)
	incremento := 0
	for i := 0 ; i < contador ; i++ {
		go func() {
			v := incremento
			runtime.Gosched()
			v++
			incremento = v
			fmt.Println(incremento)
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("El valor final de incremento: ", incremento)
}

package main

import "fmt"

type persona struct {
	Nombre string
	Apellido string
	Empleo string
	Edad int
}

func (p *persona) hablar()  {
	fmt.Printf("Hola caballeros, me llamo %v %v, tengo %v anios y soy el puto amo en %v \n", p.Nombre, p.Apellido, p.Edad, p.Empleo)
}

type humano interface {
	hablar()
}

func diAlgo(h humano)  {
	h.hablar()
}

func main()  {
	p1 := persona{
		Nombre: "Pepito",
		Apellido: "Garcia",
		Empleo: "Fontaneria",
		Edad: 22,
	}

	p2 := persona{
		Nombre: "Jose luis",
		Apellido: "Alvarez",
		Empleo: "Electricidad",
		Edad: 25,
	}
	diAlgo(&p2)

	p1.hablar()
}

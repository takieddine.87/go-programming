package main

import "fmt"

func main()  {
	ca := make(chan int)
	//enviar
	go func() {
		for i := 0; i < 5 ; i++{
			ca <- i
		}
		close(ca)
	}()

	for v := range ca {
		fmt.Println(v)
	}

	fmt.Println("Finalizando")
}


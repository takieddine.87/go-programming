package main

import "fmt"

func main()  {
	ca := make(chan int)
	//enviar
	go enviar(ca)
	
	//recibir: No hace falta que sea una goroutine ya que es simplemento es una llamada a una funcion
	//que espera que enviar(ca) tenga un valor que enviar
	recibir(ca)
	
	fmt.Println("Finalizando")
}

func enviar(c chan<- int)  {
	c <- 42
}

func recibir(c <-chan int)  {
	fmt.Println(<-c)
}
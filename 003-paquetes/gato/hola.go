package gato

import (
	"fmt"
)

func Hola() { //en mayuscula para que pueda ser llamada o exportada
	fmt.Println("Hola desde gato")
}

func Comen() { //en mayuscula para que pueda ser llamada o exportada
	fmt.Println("Los gatos comen pajaritos")
}

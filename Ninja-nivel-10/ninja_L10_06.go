package main

import "fmt"

func main()  {
	c := make(chan int)
	go envoyer(c)
	for v := range c{
		fmt.Println(v)
	}
}

func envoyer(c chan<- int)  {
	for i := 0 ; i<10 ; i++ {
		c<- i
	}
	close(c)
}

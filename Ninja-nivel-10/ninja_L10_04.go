package main

import (
	"fmt"
)

func main() {
	salir := make(chan int)
	c := generator(salir)

	recepcion(c, salir)

	fmt.Println("A punto de finalizar.")
}

func generator(salir chan<- int) <-chan int {
	c := make(chan int)

	go func() {
		for i := 0; i < 100; i++ {
			c <- i
		}
		close(c)
		salir <- 1
	}()
	return c
}

func recepcion(c, salir <-chan int) {
	for {
		select {
			case v := <-c:
				fmt.Println(v)
			case s, ok := <-salir:
				if !ok {
					fmt.Println("desde coma ok", ok)
					return
				} else {
					fmt.Println("desde else", s)
				}
				return
		}
	}
}

package main

import (
	"fmt"
)

func main() {
	c := gen()
	recibir(c) //Tenemos que desarrollar una funcion que recibe como parametro un canal receive only

	fmt.Println("A punto de finalizar.")
}

func gen() <-chan int { //Esta funcion devuelve un canal receive only
	c := make(chan int)
	go func() { //Si desarrollamos esta funcion sin la goroutina, dara error a causa de
		//que estamos en viando daros a ese canal pero no tenemos a nadie extrayendo
		for i := 0; i < 100; i++ {
			c <- i
		}
		close(c)
	}()

	return c
}

func recibir(c <-chan int) {
	for v := range c { //Cuando usamos range siempre tenemos que cerrar el canal en la funcion de envio
		fmt.Println(v)
	}
}

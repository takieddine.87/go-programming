package main
//Habria que aniadir el wait group
import "fmt"

func main()  {
	var c [10]chan int
	fanin := make(chan int)
	for i := 0 ; i<10 ; i++ {
		go enviando(c[i])
		go func() {
			for v := range c {
				fanin <- v
			}
			//close(fanin)
		}()
	}

	for v := range fanin{
		fmt.Println(v)
	}
}

func enviando(c chan<- int){
	for i := 0 ; i<10 ; i++ {
		c<- i
	}
}

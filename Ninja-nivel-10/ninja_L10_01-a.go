package main

import (
	"fmt"
)

func main() {
	c := make(chan int)
	go func() {
		c <- 42
	}()
	//Siempre una gorutina envia y otra recibe
	fmt.Println(<-c)
}

package main

import (
	"fmt"
)

func main() {
	par := make(chan int)
	impar := make(chan int)
	salir := make(chan bool)

	//enviar
	go enviar(par, impar, salir)

	recibir(par, impar, salir)

	fmt.Println("Finalizando.")
}

//Recibir
func recibir(p, i <-chan int, s <-chan bool) {
	for {
		select {
		case v := <-p:
			fmt.Println("Desde el canal par:", v)
		case v := <-i:
			fmt.Println("Desde el canal impar:", v)
		case <-s:
			return
		}
	}
}

//Enviar
func enviar(p, i chan<- int, s chan<- bool) {
	for j := 0; j < 100; j++ {
		if j%2 == 0 {
			p <- j
		} else {
			i <- j
		}
	}
	close(p)
	close(i)
	s <- true
}

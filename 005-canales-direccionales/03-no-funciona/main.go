package main

import "fmt"

func main() {
	ca1 := make(<-chan int, 2)//El signo chan<- significa que este canal es solo de recibo (receive-only)

	ca1 <- 42
	ca1 <- 43
	fmt.Println(<-ca1)
	fmt.Println(<-ca1)
	fmt.Println("#######################")
	fmt.Printf("%T\n", ca1)
}
